#include <msp430.h> 


/**
 * main.c
 */
int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
	
    P1DIR |= (BIT5 + BIT6 + BIT7);                            // Set P1.0 to output direction

    for (;;)
    {
        volatile unsigned int i;

        P1OUT ^= (BIT5 + BIT6 + BIT7);                          // Toggle P1.0 using exclusive-OR

        i = 50000;                              // Delay
        do (i--);
        while (i != 0);
    }
}
